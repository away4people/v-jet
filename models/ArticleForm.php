<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Article;
use app\models\Author;

/**
 * ContactForm is the model behind the contact form.
 */
class ArticleForm extends Model {

    public $author;
    public $title;
    public $text;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['author', 'title', 'text'], 'required'],
            // email has to be a valid email address
            [['author', 'title', 'text'], 'string'],
            [['author'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 40],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function addArticle() {
        if(!$this->validate()){
            return false;
        }
        $author = Author::find()
                ->where(['author_name' => $this->author])
                ->one();
        if ($author === NULL) {
            $author = new Author();
            $author->author_name = $this->author;
            $author->save();
            
        }
        $author_id = $author->author_id;     

        $article = new Article();
        $article->article_title = $this->title;
        $article->article_text = $this->text;
        $article->author_id = $author_id;
        
        if (!$article->save()) {
                return false;
            }
        
        return true;
    }

}

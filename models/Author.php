<?php

namespace app\models;

use yii\db\ActiveRecord;

class Author extends ActiveRecord
{
    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName()
    {
        return '{{authors}}';
    }
    
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['author_id' => 'author_id']);
    }
    
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['author_id' => 'author_id']);
    }
    
    
    
}


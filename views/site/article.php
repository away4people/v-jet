<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>    
            <?php
            echo html::encode($article->article_title);
            ?>
        </h1>
    </div>

    <div class="body-content">
        <div class="row">
            <?php
            echo "<p>" . html::encode($article->article_text) . "</p>";
            echo "<p><b>Добавлено:</b> " . html::encode($article->created_at) . "</p>";
            echo "<p><b>Автор:</b> " . html::encode($article->author['author_name']) . "</p>";
            echo "<hr><br>";

            echo "<h4>Комментарии читателей: </h4> <br>";

            foreach ($comments as $comment) {
                echo "<div style='border-bottom: 1px dashed black; border-right:1px solid black; margin:2%;'>";
                echo "<p><i>Автор:</i> " . html::encode($comment->author->author_name) . "</p>";
                echo "<p>" . html::encode($comment->comment_text) . "</p>";
                echo "<p><i>Добавлено: </i>" . html::encode($comment->created_at) . "</p><br>";
                
                // ищем ответы на комментарий
                if(array_key_exists($comment->comment_id, $replies)){
                       echo "<div style='margin-left: 5%;'>";
                        echo "<i>Ответил:</i> " . html::encode($replies[$comment->comment_id]->author->author_name) . "<br>";
                        echo html::encode($replies[$comment->comment_id]->comment_text);
                        echo "<br>";
                        echo "<i>Когда: </i>" . html::encode($replies[$comment->comment_id]->created_at);
                        echo "</div>";
                        echo "</div>";
                        continue;
                }
                // если не нашли, отображаем форму ответа для комментария
                if ($comment->reply_to_comment_id === NULL) {
                    $form = ActiveForm::begin(['id' => 'reply-form', 'options' => ['style' => 'width: 90%;
                                            font-size: 10px;
                    ']]);
                    echo $form->field($model, 'author')->textInput(['autofocus' => false]);
                    echo $form->field($model, 'text')->textarea(['rows' => 2]);
                    echo $form->field($model, 'reply_to')->hiddenInput(['value' => html::encode($comment->comment_id)])->label(false);
                    ?>

                    <div class="form-group">
                    <?= Html::submitButton('Прокомментировать', ['class' => 'btn btn-primary', 'name' => 'reply-button']) ?>
                    </div>

                    <?php
                    $form->errorSummary($model);
                    ActiveForm::end();
                }

                echo "</div>";
            }

            echo LinkPager::widget([
                'pagination' => $pagination,
            ]);
            ?>

        </div>


        <div style="margin:5%; ">

            <h3>Добавьте свой комментарий к статье</h3>
            <?php $form = ActiveForm::begin(['id' => 'comment-form']); ?>

            <?= $form->field($model, 'author')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>



            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>

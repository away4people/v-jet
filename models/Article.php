<?php

namespace app\models;

use yii\db\ActiveRecord;

class Article extends ActiveRecord
{
 
    public static function tableName()
    {
        return '{{articles}}';
    }
    
      public function beforeSave($insert)
    {
        if ($insert) {
            $this->created_at = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }
    
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['author_id' => 'author_id']);
    }
    
   public function getComments()
    {
        return $this->hasMany(Comment::className(), ['article_id' => 'article_id']);
    }
    
    
}


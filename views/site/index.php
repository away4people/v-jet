<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><a href='https://docs.google.com/document/d/17PDF2B58dDnyj_d8xqtR_FskBLB1bnQOQDVBqW9g-FA/edit'>ТЗ</a></h1>

    </div>

    <div class="body-content">


            <?php
            echo "<h3 style='text-align:center;'>ТОП-5 комментируемых статей: </h3> <hr> <div class='owl-carousel'>";
            foreach($queryMostPopular as $popularPost){
                echo ""; 
                echo "<div><p style='
                    font-size: 10px;
                    font-style: italic;
                    font-weight: bold;'
                    >Добавлено: " .
                html::encode($popularPost->created_at) . "</p>";
                echo "<h3><a href='".Url::to(['site/get-article', 'id' => html::encode($popularPost->article_id)])."'>" . html::encode($popularPost->article_title) . "</a></h3>";

                echo "<br><p>" . mb_substr(html::encode($popularPost->article_text), 0, 100) . "..</p>";
                echo "<p>Автор: " . html::encode($popularPost->author['author_name']) . "</p>";
                echo "<p>Комментариев: ".html::encode(count($popularPost->comments)) . "</p></div>";
            }
            echo "</div><hr><h3 style='text-align:center;'>Все статьи: </h3>";
            foreach ($posts as $post) {
                echo "<hr>";
                echo "<p style='float: right; float: right;
                    font-size: 10px;
                    font-style: italic;
                    font-weight: bold;'
                    >Добавлено: " .
                $post->created_at . "</p>";
                echo "<h3><a href='".Url::to(['site/get-article', 'id' => html::encode($post->article_id)])."'>" . html::encode($post->article_title) . "</a></h3>";

                echo "<br><p>" . mb_substr(html::encode($post->article_text), 0, 100) . "..</p>";
                echo "<p>Автор: " . html::encode($post->author['author_name']) . "</p>";
                echo "<p>Комментариев: ".html::encode(count($post->comments)) . "</p>";
            }

            echo LinkPager::widget([
                'pagination' => $pagination,
            ]);
            ?>


        <hr>

        <h3>Опубликуйте свои мысли</h3>
<?php $form = ActiveForm::begin(['id' => 'article-form']); ?>

        <?= $form->field($model, 'author')->textInput(['autofocus' => false]) ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>



        <div class="form-group">
<?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'article-button']) ?>
        </div>

<?php ActiveForm::end(); ?>

    </div>
</div>


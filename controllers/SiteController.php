<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Article;
use app\models\ArticleForm;
use app\models\Comment;
use app\models\CommentForm;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                        [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        // Регистрируем модель формы статей и пробуем заполнить её данными
        // В случае успеха данные валидируем, сохраняем и обновляем страницу
        $model = new ArticleForm();
        if ($model->load(Yii::$app->request->post()) && $model->addArticle()) {
            return $this->refresh();
        }

        // Находим ТОП-5 комментируемых статей
        $queryMostPopular = Article::find()
                ->select([
                    '{{articles}}.*',
                ])
                ->joinWith('author')
                ->joinWith('comments')
                ->groupBy('{{articles}}.article_id')
                ->orderBy('COUNT({{comments}}.article_id) desc')
                ->limit(5)
                ->all();

        // Находим все остальные статьи в порядке даты добавления (включая ТОП-5)
        $query = Article::find()
                ->select([
                    '{{articles}}.*',
                    'COUNT({{comments}}.article_id)'
                ])
                ->joinWith('author')
                ->joinWith('comments')
                ->groupBy('{{articles}}.article_id');

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);
        $posts = $provider->getModels();
        $pagination = $provider->getPagination();

        return $this->render('index', compact('posts', 'pagination', 'model', 'queryMostPopular'));
    }
    
    

    public function actionGetArticle() {
        $id = Yii::$app->getRequest()->getQueryParam('id');
        // Регистрируем сценарий ответов в модели и пробуем заполнить полученными данными
        // В случае успеха данные валидируем, сохраняем и обновляем страницу
        $model = new CommentForm(['scenario' => CommentForm::SCENARIO_REPLY]);
        if ($model->load(Yii::$app->request->post()) && $model->addComment($id)) {
            return $this->refresh();
        }
        // Регистрируем сценарий комментариев в модели и пробуем заполнить полученными данными
        // В случае успеха данные валидируем, сохраняем и обновляем страницу
        $model = new CommentForm(['scenario' => CommentForm::SCENARIO_COMMENT]);
        if ($model->load(Yii::$app->request->post()) && $model->addComment($id)) {
            return $this->refresh();
        }

        // Выборка статей со связью авторов (кто написал)
        $article = Article::find()
                ->joinWith('author')
                ->where(['articles.article_id' => $id])
                ->one();
        // Выборка комментариев со связью авторов (кто оставил)
        $comments = Comment::find()
                ->joinWith('author')
                ->where(['comments.article_id' => $id])
                ->andWhere('{{comments}}.reply_to_comment_id IS NULL');
        // Выборка ответов на комментарии со связью авторов (кто оставил)
        $replies = Comment::find()
                ->joinWith('author')
                ->where(['comments.article_id' => $id])
                ->andWhere('{{comments}}.reply_to_comment_id IS NOT NULL')
                ->indexBy('reply_to_comment_id')
                ->all();


        $provider = new ActiveDataProvider([
            'query' => $comments,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);
        $comments = $provider->getModels();
        $pagination = $provider->getPagination();

        return $this->render('article', compact('article', 'model', 'comments', 'pagination', 'replies'));
    }

}

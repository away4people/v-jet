<?php

namespace app\models;

use yii\base\Model;
use app\models\Author;
use app\models\Comment;


class CommentForm extends Model {

    public $author;
    public $title;
    public $text;
    public $reply_to;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['author', 'text'], 'required', 'on' => self::SCENARIO_COMMENT],
            [['author', 'text', 'reply_to'], 'required', 'on' => self::SCENARIO_REPLY],
            // email has to be a valid email address
            [['author'], 'string', 'max' => 20],
            [['text'], 'string', 'max' => 255],
        ];
    }

    const SCENARIO_COMMENT = 'comment';
    const SCENARIO_REPLY = 'reply';

    public function scenarios()
    {
        return [
            self::SCENARIO_COMMENT => ['author', 'text'],
            self::SCENARIO_REPLY => ['author', 'text', 'reply_to'],
        ];
    }
    
     

   
    public function addComment($article_id) { 
        if(!$this->validate()){
            return false;
        }
            $author_id = Author::find()
                    ->where(['author_name' => $this->author])
                    ->one();
            // в случае, если автор комментарий не был найден по имеющимся именам, регистрируем нового
            if ($author_id === NULL) {
                $author = new Author();
                $author->author_name = $this->author;
                $author->save();
                if (!$author->save()) {
                    $this->addError('text', 'Save was interrupted.');
                    return false;
                }
                $author_id = $author->author_id;
            }

            // регистрируем сам комментарий / ответ (в зависимости от сценария)
            $comment = new Comment();
            $comment->comment_text = $this->text;
            $comment->author_id = $author_id;
            $comment->article_id = $article_id;
            if($this->scenario == self::SCENARIO_REPLY){
                $comment->reply_to_comment_id = $this->reply_to;
                
            }
            // в случае сбоя при сохранении данных в БД, регистрируем ошибку
            if (!$comment->save()) {
                $this->addError('text', 'Save was interrupted.');
                return false;
            }
        return true;
    }

}

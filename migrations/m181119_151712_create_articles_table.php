<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m181119_151712_create_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles', [
            'article_id' => $this->primaryKey(),
            'article_title' => $this->string()->notNull(),
            'article_text' => $this->text()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles');
    }
}

<?php

namespace app\models;

use yii\db\ActiveRecord;

class Comment extends ActiveRecord
{
 
    public static function tableName()
    {
        return '{{comments}}';
    }
    
     public function beforeSave($insert)
    {
        if ($insert) {
            $this->created_at = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }
    

    
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['author_id' => 'author_id']);
    }
    
    
}


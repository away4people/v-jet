<?php

use yii\db\Migration;

/**
 * Handles the creation of table `authors`.
 */
class m181119_151700_create_authors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('authors', [
            'author_id' => $this->primaryKey(),
            'author_name' => $this->string()->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('authors');
    }
}
